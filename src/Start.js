import React, { Component } from "react";
import Navbar from "./pages/navbar";
class Start extends Component {
  render() {
    const { children } = this.props;
    return (
      <div>
        <Navbar />
        {children}
      </div>
    );
  }
}
export default Start;
