import React, { Component } from 'react';
import {
	Table,
	Button,
	Input,
	Row,
	Col,
	Container,
	FormGroup,
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Label,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';

import './homepage.css';
import { get, post } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';

export default class homepage extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);

		this.state = {
			isOpen: false,
			dataAll: [],
			status: 1,
			currentPage: 0
		};
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	componentDidMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
		this.getdataAll(1);
	}
	async getdataAll(e) {
		let check = Number(e);
		let repair = await get('/getrepair');
		let request = await get('/getrequest');
		let data = repair.result.concat(request.result);
		this.setState({
			dataAll: data
				.sort((a, b) => moment(b.date) - moment(a.date))
				.map((e, i) => {
					let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
					let time = moment(e.date).format('HH:mm');
					let cname = 'scb' + e.cname;
					return {
						id: e.rpid || e.rqid,
						date,
						time,
						cname,
						type: e.type,
						name: e.fname + ' ' + e.lname,
						status: e.status,
						amount: e.amount || '-'
					};
				})
				.filter((e) => e.status === 'รอดำเนินการ' || e.status === 'กำลังดำเนินการ'),
			status: check
		});
	}
	searchText(e) {
		let { status } = this.state;
		this.getdataAll(status).then(() => {
			let { dataAll } = this.state;
			let texts = e.toLowerCase();
			let a = dataAll.filter(
				(el) =>
					el.date.toLowerCase().indexOf(texts) > -1 ||
					el.time.toLowerCase().indexOf(texts) > -1 ||
					el.cname.toLowerCase().indexOf(texts) > -1 ||
					el.type.toLowerCase().indexOf(texts) > -1 ||
					el.name.toLowerCase().indexOf(texts) > -1 ||
					el.status.toLowerCase().indexOf(texts) > -1 ||
					String(el.amount).toLowerCase().indexOf(texts) > -1
			);
			this.setState({ dataAll: a, currentPage: 0 });
		});
	}
	handleClick(e, index) {
		e.preventDefault();

		this.setState({
			currentPage: index
		});
	}
	render() {
		let { dataAll, status, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(dataAll.length > 0 && dataAll.length / pageSize);
		return (
			<div id="formadmin" style={{ backgroundColor: '#f8f8f8' }}>
				<Navbar light expand="md" style={{ backgroundColor: ' #6699cc' }}>
					<NavbarBrand href="/" style={{ fontSize: '1.50rem', fontWeight: '700', color: 'white' }}>
						IT services
					</NavbarBrand>

					<Nav className="ml-auto" navbar>
						<NavItem>
							<NavLink href="/login" style={{ color: 'white', border: '5px' }}>
								Login
							</NavLink>
						</NavItem>
					</Nav>
				</Navbar>
				<Container style={{ paddingTop: '1rem' }}>
					<Row id="admincontainer2" style={{ width: '90%' }}>
						<Col xs={12}>
							<h1>Online appliance repair service</h1>
						</Col>
						<Col xs={12}>
							<h3>ระบบแจ้งซ่อมอุปกรณ์ออนไลน์</h3>
						</Col>
					</Row>
				</Container>
				<Container style={{ paddingTop: '1rem' }}>
					<Row id="admincontainer" style={{ width: '90%' }}>
						<Col sm={6} />
						<Col sm={6}>
							<Input
								type="search"
								placeholder="search placeholder"
								onChange={(e) => this.searchText(e.target.value)}
							/>
						</Col>
						<Col sm={12}>
							<div>
								<Table striped responsive>
									<thead>
										<tr>
											<th>วันที่</th>
											<th>เวลา</th>
											<th>ประเภท</th>
											<th>สถานที่</th>
											<th>ชื่อผู้แจ้งปัญหา</th>
											<th>สถานะ</th>
											<th>จัดการ</th>
										</tr>
									</thead>
									<tbody>
										{dataAll.length > 0 &&
											dataAll
												.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
												.map((e) => (
													<tr>
														<td>{e.date}</td>
														<td>{e.time}</td>
														<td>{e.type}</td>
														<td>{e.cname}</td>
														<td>{e.name}</td>
														<td
															style={
																e.status == 'รอดำเนินการ' ? (
																	{ color: '#3893f5' }
																) : e.status == 'เสร็จสิ้น' ? (
																	{ color: 'green' }
																) : e.status == 'กำลังดำเนินการ' ? (
																	{ color: 'blue' }
																) : null
															}
														>
															{e.status}
														</td>

														<td>
															<Button
																color="success"
																style={{
																	width: '120px',
																	height: '30px',
																	padding: '2px 2px 2px 2px'
																}}
																href={
																	(e.status == 'รอดำเนินการ'
																		? '/detailWork_homepage/'
																		: '/detailWork_homepage2/') +
																	(e.type == 'แจ้งซ่อม' ? 1 : 2) +
																	'/' +
																	e.id
																}
															>
																รายละเอียด
															</Button>
														</td>
													</tr>
												))}
									</tbody>
								</Table>
							</div>
						</Col>
						<Col sm={12}>
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
