import React, { Component } from 'react';

import {
	Button,
	Label,
	Row,
	Col,
	Container,
	Alert,
	Modal,
	ModalBody,
	ModalHeader,
	CardImg,
	Table,
	Card,
	Navbar,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink
} from 'reactstrap';
import '../man/manre3.css';
import { get, post,sever } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
// let api = sever+'/image/repair/';
let api = sever+'/image/repair/';

export default class detailWork_homepage2 extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataAll: [],
			dataDraw: [],
			dataTool: [],
			check: '',
			role: ''
		};
	}

	async componentDidMount() {
		let type = this.props.match.params.type;
		if (type == 1) {
			this.getrepair_man_by_id();
			this.getDraw_repair();
		} else {
			this.getrequest_man_by_id();
			this.getDraw_request();
		}
	}
	getrepair_man_by_id = async () => {
		let res = await post('/getrepair_man_by_id', { id: this.props.match.params.id });
		console.log('res', res);
		this.setState({
			dataAll: res.result
		});
	};
	getrequest_man_by_id = async () => {
		let res = await post('/getrequest_man_by_id', { id: this.props.match.params.id });
		this.setState({
			dataAll: res.result
		});
	};
	getDraw_repair = async () => {
		let tool = await get('/gettools');
		let res = await post('/getdraw_repair', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid)),
			dataTool: res.result.map((e) => {
				let a = Number(tool.result.filter((el) => el.id == e.tid)[0].amount) - Number(e.amount);
				return { id: e.tid, amount: Number(a) };
			}),
			check: res.result
				.map((e) => {
					let a =
						Number(tool.result.filter((el) => el.id == e.tid)[0].amount) >= Number(e.amount) ? true : false;
					return a;
				})
				.find((e) => e == false)
		});
	};
	getDraw_request = async () => {
		let tool = await get('/gettools');
		let res = await post('/getdraw_request', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid)),
			dataTool: res.result.map((e) => {
				let a = Number(tool.result.filter((el) => el.id == e.tid)[0].amount) - Number(e.amount);
				return { id: e.tid, amount: Number(a) };
			}),
			check: res.result
				.map((e) => {
					let a =
						Number(tool.result.filter((el) => el.id == e.tid)[0].amount) >= Number(e.amount) ? true : false;
					return a;
				})
				.find((e) => e == false)
		});
	};
	submit = () => {
		let { check } = this.state;
		// console.log('check', check);
		if (check == undefined) {
			swal({
				title: 'Are you sure?',
				text: 'ยืนยันการทำรายการต่อหรือไม่',
				icon: 'warning',
				buttons: true,
				dangerMode: true
			}).then(async (willDelete) => {
				if (willDelete) {
					let { type, id } = this.props.match.params;
					if (type == 1) {
						let res = await post('/repair_success', { id });
						if (res.success) {
							this.state.dataTool.forEach(async (e, i) => {
								await post('/update_tools', e);
							});
							swal('สำเร็จ!', 'เสร็จสิ้น', 'success', {
								buttons: false,
								timer: 1500
							}).then(() => {
								this.props.history.push('/man_re1');
							});
						} else {
							swal('ผิดพลาด!', 'ยืนยันไม่สำเร็จ', 'error', {
								buttons: false,
								timer: 1500
							});
						}
					} else {
						let res = await post('/request_success', { id });
						if (res.success) {
							this.state.dataTool.forEach(async (e, i) => {
								await post('/update_tools', e);
							});
							swal('สำเร็จ!', 'เสร็จสิ้น', 'success', {
								buttons: false,
								timer: 1500
							}).then(() => {
								this.props.history.push('/man_re1');
							});
						} else {
							swal('ผิดพลาด!', 'ยืนยันไม่สำเร็จ', 'error', {
								buttons: false,
								timer: 1500
							});
						}
					}
				}
			});
		} else {
			swal('ไม่สามารถดำเนินการได้!', 'กรุณาจัดการข้อมูลการเบิกใหม่อีกครั้ง', 'warning');
		}
	};
	render() {
		let { dataAll, dataDraw, role } = this.state;
		// console.log('dataDraw', dataDraw);
		return (
			<div
				id="form12"
				style={{
					backgroundColor: '#f8f8f8'
				}}
			>
				<Navbar light expand="md" style={{ backgroundColor: ' #6699cc' }}>
					<NavbarBrand href="/" style={{ fontSize: '1.50rem', fontWeight: '700', color: 'white' }}>
						IT services
					</NavbarBrand>

					<Nav className="ml-auto" navbar>
						<NavItem>
							<NavLink href="/login" style={{ color: 'white', border: '5px' }}>
								Login
							</NavLink>
						</NavItem>
						{/* <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  เข้าสูระบบ
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>Option 1</DropdownItem>
                  <DropdownItem>Option 2</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Reset</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown> */}
					</Nav>
				</Navbar>
				<Container
					style={{
						paddingTop: '3rem'
					}}
				>
					{dataAll.length > 0 &&
						dataAll.map((e) => (
							<Row
								xs={12}
								sm={6}
								style={{
									width: '90%',
									backgroundColor: 'white',
									padding: '0.30rem 0.30rem 0.30rem 0.30rem',
									borderRadius: '1rem'
								}}
							>
								<Col xs={12} sm={6}>
									<Col
										xs={12}
										sm={12}
										style={{ backgroundColor: 'white', padding: '2px 2px 2px 2px' }}
									>
										<Alert color="info">รายการเบิกอุปกรณ์</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ผู้ดำเนินการซ่อม</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.fname_m + ' ' + e.lname_m}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>สถานะ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{' '}
														{e.status}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>วันที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{moment(e.date).format('DD MMMM') +
															' ' +
															(Number(moment(e.date).format('YYYY')) + 543)}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>เวลา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{moment(e.date).format('HH:mm')}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ประเภท</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.type}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>สถานที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{'scb' + e.cname}
													</Label>
												</Col>
											</Col>
										</Row>
										<hr />
										<Row>
											<Col sm={6}>
												<Col>
													<Label>คำขออุปกรณ์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{this.props.match.params.type == 1 ? '-' : e.device_problem}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>จำนวณ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{this.props.match.params.type == 1 ? '-' : e.amount}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>อุปกรณ์ที่เกิดปัญหา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{this.props.match.params.type == 1 ? e.device_problem : '-'}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>รายละเอียดงาน</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															padding: '0.5rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.detail}
													</Label>
												</Col>
											</Col>
										</Row>
									</Col>
								</Col>
								<Col xs={12} sm={6}>
									<Col xs={12} style={{ backgroundColor: 'white', padding: '2px 0px 0px 2px' }}>
										<Alert color="info">ข้อมูลผู้ใช้งาน</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ชื่อ-สกุล</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.fname_u + ' ' + e.lname_u}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>ตำแหน่ง</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.position}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สาขาวิชา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.branch}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>โทรศัพท์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.phone}
													</Label>
												</Col>
											</Col>
										</Row>
									</Col>
									<Col
										xs={12}
										style={{
											backgroundColor: 'white',
											padding: '2px 2px 2px 2px'
										}}
									>
										<Alert color="info">รูปภาพ</Alert>
										<Col sm={12}>
											<Card inverse style={{ textAlign: 'center' }}>
												{this.props.match.params.type == 1 ? (
													<CardImg
														style={{ width: 'auto', height: '13rem' }}
														src={api + this.props.match.params.id + '.png'}
													/>
												) : (
													<CardImg
														style={{ width: 'auto', height: '13rem' }}
														src={require('../assets/image/default-image.jpg')}
													/>
												)}
											</Card>
										</Col>
									</Col>
								</Col>
								<div id="btn456">
									<Row>
										<Col
											xs={6}
											style={{
												textAlign: 'right'
											}}
										>
											<Button
												color="danger"
												style={{
													width: '8rem'
												}}
												onClick={() => this.props.history.goBack()}
											>
												ยกเลิก
											</Button>
										</Col>
										<Col xs={6} />
									</Row>
								</div>
							</Row>
						))}
				</Container>
			</div>
		);
	}
}
