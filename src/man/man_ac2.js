import React, { Component } from 'react';
import { Table, Input, Row, Col, Container, FormGroup } from 'reactstrap';

export default class man_ac2 extends Component {
	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
		}
	}
	render() {
		return (
			<div id="form" style={{ backgroundColor: '#f8f8f8' }}>
				<Container style={{ paddingTop: '3rem' }}>
					<Row id="container1" style={{ width: '90%' }}>
						<Col xs={12} sm={6}>
							<FormGroup>
								<Input type="select" name="select">
									<option>ประวัติการเบิก</option>
									<option>อะไหล่และอุปกรณ์เหลือ</option>
								</Input>
							</FormGroup>
						</Col>

						<Col xs={12} sm={6}>
							<Input type="search" name="search" id="exampleSearch" placeholder="search" />
						</Col>

						<Col>
							<div style={{ paddingTop: '1rem' }}>
								<Table striped responsive>
									<thead>
										<tr>
											<th>id</th>
											<th>วันที่</th>
											<th>เวลา</th>
											<th>รูปภาพ</th>
											<th>ประเภท</th>
											<th>ชื่ออะไหล่และอุปกรณ์</th>
											<th>จำนวนคงเหลือ</th>
										</tr>
										<tr>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
											<td>1</td>
										</tr>
									</thead>
									<tbody />
								</Table>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
