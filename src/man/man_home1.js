import React, { Component } from 'react';
import {
	Table,
	Button,
	Input,
	Row,
	Col,
	Container,
	FormGroup,
	Modal,
	ModalBody,
	ModalHeader,
	Label,
	Card,
	CardImg,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import { get, post } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import { async } from 'q';
export default class man_home1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			dataAll: [],
			dataModal: [],
			selectRe: 1,
			currentPage: 0
		};
		this.toggle = this.toggle.bind(this);
	}
	toggle(e) {
		let a = [];
		a.push(e);
		this.setState((prevState) => ({
			modal: !prevState.modal,
			dataModal: a
		}));
	}
	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getdataRepair();
		}
	}
	getdataRepair = async () => {
		let res = await get('/getrepair_man');
		this.setState({
			dataAll: res.result.map((e, i) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rpid: e.rpid,
					date,
					time,
					cname,
					device_problem: e.device_problem,
					detail: e.detail,
					status: e.status
				};
			})
		});
	};
	getdataRequest = async () => {
		let res = await get('/getrequest_man');
		// console.log('res', res);
		this.setState({
			dataAll: res.result.map((e, i) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rqid: e.rqid,
					date,
					time,
					cname,
					device_problem: e.device_problem,
					detail: e.detail,
					status: e.status,
					amount: e.amount
				};
			})
		});
	};
	searchText(e) {
		let { selectRe } = this.state;
		if (selectRe == 1) {
			this.getdataRepair().then(() => {
				let { dataAll } = this.state;
				let texts = e.toLowerCase();
				let a = dataAll.filter(
					(el) =>
						el.date.toLowerCase().indexOf(texts) > -1 ||
						el.time.toLowerCase().indexOf(texts) > -1 ||
						el.cname.toLowerCase().indexOf(texts) > -1 ||
						el.device_problem.toLowerCase().indexOf(texts) > -1 ||
						el.detail.toLowerCase().indexOf(texts) > -1 ||
						el.status.toLowerCase().indexOf(texts) > -1
				);
				// console.log('data', a);
				this.setState({ dataAll: a, currentPage: 0 });
			});
		} else {
			this.getdataRequest().then(() => {
				let { dataAll } = this.state;
				let texts = e.toLowerCase();
				let a = dataAll.filter(
					(el) =>
						el.date.toLowerCase().indexOf(texts) > -1 ||
						el.time.toLowerCase().indexOf(texts) > -1 ||
						el.cname.toLowerCase().indexOf(texts) > -1 ||
						el.device_problem.toLowerCase().indexOf(texts) > -1 ||
						el.detail.toLowerCase().indexOf(texts) > -1 ||
						el.status.toLowerCase().indexOf(texts) > -1 ||
						el.amount.toString().toLowerCase().indexOf(texts) > -1
				);
				// console.log('data', a);
				this.setState({ dataAll: a, currentPage: 0 });
			});
		}
	}
	handleClick(e, index) {
		e.preventDefault();

		this.setState({
			currentPage: index
		});
	}
	render() {
		let { dataAll, dataModal, selectRe, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(dataAll.length > 0 && dataAll.length / pageSize);
		const closeBtn = (
			<button className="close" style={{ fontsize: '1rem', color: 'white' }} onClick={this.toggle}>
				Close
			</button>
		);

		return (
			<div id="form" style={{ backgroundColor: '#f8f8f8' }}>
				<Container style={{ paddingTop: '3rem' }}>
					<Row id="container1" style={{ width: '90%' }}>
						<Col xs={12} sm={6}>
							<FormGroup>
								<Input
									type="select"
									value={selectRe}
									onChange={(e) =>
										this.setState({ selectRe: e.target.value }, () => {
											let { selectRe } = this.state;
											if (selectRe == 1) {
												this.getdataRepair();
											} else {
												this.getdataRequest();
											}
										})}
								>
									<option value={1}>รายการซ่อม</option>
									<option value={2}>รายการขออุปกรณ์</option>
								</Input>
							</FormGroup>
						</Col>

						<Col xs={12} sm={6}>
							<Input
								type="search"
								placeholder="ค้นหารายการ"
								onChange={(e) => this.searchText(e.target.value)}
								// style={{ float: "right" }}
							/>
						</Col>

						<Col sm={12}>
							<div style={{ paddingTop: '1rem' }}>
								<Table striped responsive>
									<thead>
										<tr>
											<th>วันที่</th>
											<th>เวลา</th>
											<th>สถานที่</th>
											<th>อุปกรณ์ที่เกิดปัญหา</th>
											<th>ปัญหาที่พบ</th>
											{selectRe == 2 && <th>จำนวน</th>}
											<th>สถานะ</th>
											<th>จัดการ</th>
										</tr>
									</thead>
									<tbody>
										{dataAll.length != 10 &&
											dataAll
												.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
												.map((el) => (
													<tr>
														<td>{el.date}</td>
														<td>{el.time}</td>
														<td>{el.cname}</td>
														<td>{el.device_problem}</td>
														<td>{el.detail}</td>
														{selectRe == 2 && <td>{el.amount}</td>}
														<td
															style={
																el.status == 'รอดำเนินการ' ? (
																	{ color: '#3893f5', width: '170px' }
																) : el.status == 'เสร็จสิ้น' ? (
																	{ color: 'green', width: '170px' }
																) : el.status == 'กำลังดำเนินการ' ? (
																	{ color: 'blue', width: '170px' }
																) : null
															}
														>
															{el.status}
														</td>
														<td>
															<Row>
																<Col xs={12} sm={12} xl={6}>
																	<Button
																		color="success"
																		size="sm"
																		href={
																			'/detailWork/' +
																			(selectRe == 1 ? 1 : 2) +
																			'/' +
																			(selectRe == 1 ? el.rpid : el.rqid)
																		}
																		style={{
																			width: '90px'
																		}}
																	>
																		รายละเอียด
																	</Button>
																</Col>
															</Row>
														</td>
													</tr>
												))}
									</tbody>
								</Table>
							</div>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
				{/* <Modal
					isOpen={this.state.modal}
					toggle={this.toggle}
					className={this.props.className}
					style={{
						width: '95%',
						display: 'flex',
						justifyContent: 'center'
					}}
				>
					<ModalHeader
						close={closeBtn}
						style={{
							backgroundColor: '#6699cc',
							color: 'white'
						}}
					>
						รายละเอียด
					</ModalHeader>

					{dataModal.length != 0 &&
						dataModal.map((el) => (
							<ModalBody>
								<div>
									<Row>
										<Col sm={4}>
											<Label>วันที่</Label>
											<br />
											<Label
												style={{
													backgroundColor: '#f2f2f2',
													width: '100%',
													height: '1.75rem',
													textAlign: 'center',
													borderRadius: '0.25rem'
												}}
											>
												{el.date}
											</Label>
										</Col>

										<Col sm={4}>
											<Label>เวลา</Label>
											<br />
											<Label
												style={{
													backgroundColor: '#f2f2f2',
													width: '100%',
													height: '1.75rem',
													textAlign: 'center',
													borderRadius: '0.25rem'
												}}
											>
												<td>{el.time}</td>
											</Label>
										</Col>
										<Col sm={4}>
											<Label>สถานะ</Label>
											<br />
											<Label
												style={
													el.status == 'รอดำเนินการ' ? (
														{ color: '#3893f5' }
													) : el.status == 'เสร็จสิ้น' ? (
														{ color: 'green' }
													) : (
														{ color: 'red' }
													)
												}
												style={{
													backgroundColor: '#f2f2f2',
													width: '100%',
													height: '1.75rem',
													textAlign: 'center',
													borderRadius: '0.25rem'
												}}
											>
												{el.status}
											</Label>
										</Col>
									</Row>
									<Row>
										<Col sm={4}>
											<Label>สถานที่</Label>
											<br />
											<Label
												style={{
													backgroundColor: '#f2f2f2',
													width: '100%',
													height: '1.75rem',
													textAlign: 'center',
													borderRadius: '0.25rem'
												}}
											>
												{el.cname}
											</Label>
										</Col>
										<Col sm={4}>
											<Label>อุปกรณ์ที่เกิดปัญหา</Label>
											<br />
											<Label
												style={{
													backgroundColor: '#f2f2f2',
													width: '100%',
													height: '1.75rem',
													textAlign: 'center',
													borderRadius: '0.25rem'
												}}
											>
												{el.tname}
											</Label>
										</Col>
									</Row>
									<Row>
										<Col sm={6}>
											<Label>ปัญหาที่พบ</Label>
											<br />
											<Label
												style={{
													backgroundColor: '#f2f2f2',
													width: '100%',
													height: '1.75rem',
													textAlign: 'center',
													borderRadius: '0.25rem',
													height: '5rem',
													textAlign: 'left',
													padding: ' 0.75rem 0.75rem 0.75rem 0.75rem'
												}}
											>
												{el.detail}
											</Label>
										</Col>
										<Col sm={6}>
											<Label />
											<Card inverse style={{ textAlign: 'center' }}>
												<CardImg
													src={'http://localhost:3001/image/repair/' + el.rpid + '.png'}
												/>
											</Card>
										</Col>
									</Row>
									<Row>
										<Col style={{ textAlign: 'center' }}>
											<Button
												color="success"
												onClick={() => {
													this.updateStatus(el.rpid);
													this.setState({ modal: false });
												}}
											>
												ยืนยัน
											</Button>
										</Col>
									</Row>
								</div>
							</ModalBody>
						))}
				</Modal> */}
			</div>
		);
	}
}
