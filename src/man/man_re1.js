import React, { Component } from 'react';
import {
	Table,
	Button,
	Input,
	Row,
	Col,
	Container,
	FormGroup,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import swal from 'sweetalert';
import { get, post } from '../service/service';
import moment from 'moment';

export default class man_re1 extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataTable: [],
			currentPage: 0
		};
	}

	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getdataRepair();
			// console.log('res', res);
		}
	}
	getdataRepair = async () => {
		let USER = JSON.parse(localStorage.getItem('myData'));
		let res = await post('/getrepair_man2', { mid: USER.id });
		let ress = await post('/getrequest_man2', { mid: USER.id });
		let datas = res.result.concat(ress.result);
		// console.log('datas', datas);
		this.setState({
			dataTable: datas.sort((a, b) => moment(b.date) - moment(a.date)).map((e, i) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let cname = 'scb' + e.cname;
				return {
					rpid: e.rpid || e.rqid,
					date,
					time,
					cname,
					type: e.type,
					name: e.fname + ' ' + e.lname,
					status: e.status
				};
			})
		});
	};
	searchText(e) {
		this.getdataRepair().then(() => {
			let { dataTable } = this.state;
			let texts = e.toLowerCase();
			let a = dataTable.filter(
				(el) =>
					el.date.toLowerCase().indexOf(texts) > -1 ||
					el.time.toLowerCase().indexOf(texts) > -1 ||
					el.cname.toLowerCase().indexOf(texts) > -1 ||
					el.type.toLowerCase().indexOf(texts) > -1 ||
					el.name.toLowerCase().indexOf(texts) > -1 ||
					el.status.toLowerCase().indexOf(texts) > -1
			);
			this.setState({ dataTable: a, currentPage: 0 });
		});
	}
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { dataTable, currentPage } = this.state;
		// console.log('dataTable', dataTable);
		let pageSize = 5;
		let pagesCount = Math.ceil(dataTable.length > 0 && dataTable.length / pageSize);
		return (
			<div id="form" style={{ backgroundColor: '#f8f8f8' }}>
				<Container style={{ paddingTop: '3rem' }}>
					<Row id="container1" style={{ width: '90%' }}>
						<Col xs={12} sm={6}>
							<FormGroup>
								<h5>งานที่กำลังดำเนินการ</h5>
							</FormGroup>
						</Col>
						<Col xs={12} sm={6}>
							<Input
								type="search"
								placeholder="ค้นหารายการ"
								onChange={(e) => this.searchText(e.target.value)}
								// style={{ float: "right" }}
							/>
						</Col>
						<Col sm={12}>
							<div style={{ paddingTop: '1rem' }}>
								<Table striped responsive>
									<thead>
										<tr>
											<th>วันที่</th>
											<th>เวลา</th>
											<th>ประเภท</th>
											<th>สถานที่</th>
											<th>ชื่อผู้แจ้งปัญหา</th>
											<th>สถานะ</th>
											<th />
										</tr>
									</thead>
									<tbody>
										{dataTable.length != 0 &&
											dataTable
												.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
												.map((el) => (
													<tr>
														<td>{el.date}</td>
														<td>{el.time}</td>
														<td>{el.type}</td>
														<td>{el.cname}</td>
														<td>{el.name}</td>
														<td style={{ color: 'blue' }}>{el.status}</td>
														<td>
															<Button
																color="danger"
																style={{ width: '6rem', height: '2rem' }}
																onClick={() => this.cancelStatus(el.rpid, el.type)}
															>
																ยกเลิก
															</Button>
															<Button
																color="success"
																style={{
																	width: '6rem',
																	height: '2rem',
																	marginLeft: '5px'
																}}
																href={
																	'/man_re3/' +
																	(el.type == 'แจ้งซ่อม' ? 1 : 2) +
																	'/' +
																	el.rpid
																}
															>
																เสร็จสิ้น
															</Button>
														</td>
													</tr>
												))}
									</tbody>
								</Table>
							</div>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
	cancelStatus = async (e, t) => {
		swal({
			title: 'Are you sure?',
			text: 'คุณกำลังยกเลิกรายการนี้!',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let id = e;
				if (t == 'แจ้งซ่อม') {
					let res = await post('/updatestatus2', { id });
					if (res.success) {
						swal('สำเร็จ!', res.message, 'success', {
							buttons: false,
							timer: 1500
						}).then(() => {
							this.getdataRepair();
						});
					} else {
						swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
							buttons: false,
							timer: 1500
						});
					}
				} else {
					let res = await post('/updatestatus4', { id });
					if (res.success) {
						swal('สำเร็จ!', res.message, 'success', {
							buttons: false,
							timer: 1500
						}).then(() => {
							this.getdataRepair();
						});
					} else {
						swal('ผิดพลาด!', 'ทำรายการไม่สำเร็จ', 'error', {
							buttons: false,
							timer: 1500
						});
					}
				}
			}
		});
	};
}
