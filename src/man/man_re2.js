import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Button,
	Input,
	Table,
	Alert,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import './manre2.css';
import swal from 'sweetalert';
import { get, post } from '../service/service';
import moment from 'moment';
import { async } from 'q';

export default class man_re2 extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataTable_left: [],
			dataTable_Right: [],
			//----------------------
			data_1up: [],
			currentPage: 0
		};
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.gettools();
		}
	}
	async gettools() {
		let { id, type } = this.props.match.params;
		let res = await get('/gettools');
		let ress = await post(type == 1 ? '/getdraw_repair' : '/getdraw_request', { did: id });
		// console.log('ress', ress);
		let a = res.result.map((e) => {
			let type = e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่';
			let a = ress.result.filter((el) => el.tid == e.id);
			let putamount = a.length > 0 ? a[0].amount : 0;
			let check = a.length > 0 ? a[0].amount : 0;
			return {
				type,
				id: e.id,
				tname: e.tname,
				amount: e.amount,
				putamount: putamount,
				check: check
			};
		});
		this.setState({
			dataTable_left: a,
			dataTable_Right: ress.result
		});
	}
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	searchText(e) {
		this.gettools().then(() => {
			let { dataTable_left } = this.state;
			let texts = e.toLowerCase();
			let a = dataTable_left.filter(
				(el) =>
					el.type.toLowerCase().indexOf(texts) > -1 ||
					el.tname.toLowerCase().indexOf(texts) > -1 ||
					String(el.amount).toLowerCase().indexOf(texts) > -1
			);
			// console.log('data', a);
			this.setState({ dataTable_left: a, currentPage: 0 });
		});
	}
	submitData = async () => {
		let { id, type } = this.props.match.params;
		let { dataTable_Right, dataTable_left } = this.state;
		let USER = JSON.parse(localStorage.getItem('myData'));
		if (dataTable_Right.length > 0) {
			// console.log('dataTable_left', dataTable_left);
			let datas_old = dataTable_Right.map((e) => {
				let id = this.props.match.params.id;
				let mid = USER.id;
				let a = dataTable_left.filter((el) => el.id == e.tid);
				let putamount = a[0].putamount;
				return {
					did: Number(id), // id งาน
					tid: e.tid, // id อุปกรณ์
					mid: mid, // id ช่าง
					amount: Number(putamount),
					type: e.type,
					date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
				};
			});
			let datas_new = dataTable_left.filter((e) => e.check == 0 && e.putamount > 0).map((e, i) => {
				let id = this.props.match.params.id;
				let mid = USER.id;
				let type = e.type == 'อุปกรณ์' ? 'tool' : 'spare';
				return {
					did: Number(id), // id งาน
					tid: e.id, // id อุปกรณ์
					mid: mid, // id ช่าง
					amount: Number(e.putamount),
					type: type,
					date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
				};
			});
			if (type == 1) {
				datas_new.forEach(async (e, i) => {
					await post('/insertdraw_repair', e);
				});
				datas_old.forEach(async (e, i) => {
					if (e.amount == 0) {
						await post('/deldraw_repair', e);
					} else {
						await post('/updatedraw_repair', e);
					}
				});
				swal('สำเร็จ!', 'เพิ่มรายการเบิกเสร็จสิ้น', 'success', {
					buttons: false,
					timer: 2500
				}).then(() => {
					this.props.history.goBack();
				});
			} else {
				datas_new.forEach(async (e, i) => {
					await post('/insertdraw_request', e);
				});
				datas_old.forEach(async (e, i) => {
					if (e.amount == 0) {
						await post('/deldraw_request', e);
					} else {
						await post('/updatedraw_request', e);
					}
				});
				swal('สำเร็จ!', 'เพิ่มรายการเบิกเสร็จสิ้น', 'success', {
					buttons: false,
					timer: 2500
				}).then(() => {
					this.props.history.goBack();
				});
			}
			// console.log('datas_old', datas_old);
		} else {
			let datas = dataTable_left.filter((e) => e.putamount > 0).map((e, i) => {
				let id = this.props.match.params.id;
				let mid = USER.id;
				let type = e.type == 'อุปกรณ์' ? 'tool' : 'spare';
				return {
					did: Number(id), // id งาน
					tid: e.id, // id อุปกรณ์
					mid: mid, // id ช่าง
					amount: Number(e.putamount),
					type: type,
					date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
				};
			});
			if (type == 1) {
				datas.forEach(async (e, i) => {
					await post('/insertdraw_repair', e);
				});
				swal('สำเร็จ!', 'เพิ่มรายการเบิกเสร็จสิ้น', 'success', {
					buttons: false,
					timer: 2500
				}).then(() => {
					this.props.history.goBack();
				});
			} else {
				datas.forEach(async (e, i) => {
					await post('/insertdraw_request', e);
				});
				swal('สำเร็จ!', 'เพิ่มรายการเบิกเสร็จสิ้น', 'success', {
					buttons: false,
					timer: 2500
				}).then(() => {
					this.props.history.goBack();
				});
			}
		}
	};
	onhanDle = (e, i) => {
		let { dataTable_left } = this.state;
		if (e > dataTable_left[i].amount) {
			swal('คำเตือน!', 'เกินจำนวนที่มีอยู่', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else if (e < 0) {
			swal('คำเตือน!', 'กรุณากรอกจำนวนเต็ม', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			dataTable_left[i].putamount = e;
			this.setState({
				dataTable_left
			});
		}
	};
	render() {
		let { dataTable_left, currentPage } = this.state;
		// console.log('dataTable_left', dataTable_left);
		let pageSize = 5;
		let pagesCount = Math.ceil(dataTable_left.length > 0 && dataTable_left.length / pageSize);
		return (
			<div
				id="a"
				style={{
					backgroundColor: '#f8f8f8'
				}}
			>
				<Container>
					<Row
						style={{
							width: '90%',
							marginTop: '1rem'
						}}
					>
						<Col xs={12} sm={12}>
							<div id="bb">
								<Row xs={12}>
									<Col xs={12} sm={6}>
										<Alert color="success">
											<h5>อะไหล่และอุปกรณ์</h5>
										</Alert>
									</Col>
									<Col xs={12} sm={6}>
										<Input
											type="search"
											placeholder="ค้นหารายการ"
											onChange={(e) => this.searchText(e.target.value)}
										/>
									</Col>
								</Row>

								<div>
									<Table striped responsive>
										<thead>
											<tr>
												<th>ประเภท</th>
												<th>ชื่อ</th>
												<th>คงเหลือ</th>
												<th>ต้องการ</th>
											</tr>
										</thead>
										<tbody>
											{dataTable_left.length > 0 &&
												dataTable_left
													.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
													.map((e, i) => (
														<tr>
															<td>{e.type}</td>
															<td>{e.tname}</td>
															<td>{e.amount}</td>
															<td width={150}>
																<Input
																	value={e.putamount}
																	type="number"
																	bsSize="sm"
																	style={{ height: '1.5rem' }}
																	onChange={(e) => this.onhanDle(e.target.value, i)}
																/>
															</td>
														</tr>
													))}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
						<div id="btn444">
							<Row>
								<Col
									xs={6}
									style={{
										textAlign: 'right'
									}}
								>
									<Button
										color="danger"
										style={{
											width: '5rem'
										}}
										onClick={() => this.props.history.goBack()}
									>
										กลับ
									</Button>
								</Col>
								<Col xs={6}>
									<Button
										color="success"
										style={{
											width: '5rem'
										}}
										onClick={this.submitData}
									>
										บันทึก
									</Button>
								</Col>
							</Row>
						</div>
					</Row>
				</Container>
			</div>
		);
	}
}
