import React, { Component } from 'react';

import {
	Button,
	Label,
	Row,
	Col,
	Container,
	Alert,
	Modal,
	ModalBody,
	ModalHeader,
	CardImg,
	Table,
	Card
} from 'reactstrap';
import './manre3.css';
import { get, post, sever } from '../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import XlsExport from 'xlsexport';
// let api = sever+'/image/repair/';
let api = sever + '/image/repair/';

export default class man_home2 extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataAll: [],
			dataDraw: [],
			dataTool: [],
			check: '',
			role: '',
			line_token: ''
		};
	}

	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let USER = JSON.parse(localStorage.getItem('myData'));
			let ress = await get('/get_man');
			this.setState({ role: USER.role, line_token: ress.result });
			let type = this.props.match.params.type;
			if (type == 1) {
				this.getrepair_man_by_id();
				this.getDraw_repair();
			} else {
				this.getrequest_man_by_id();
				this.getDraw_request();
			}
		}
	}
	getrepair_man_by_id = async () => {
		let res = await post('/getrepair_man_by_id', { id: this.props.match.params.id });
		let ress = await post('/get_user_by_id', { id: res.result[0].uid });
		// console.log('ress', ress);
		this.setState({
			dataAll: res.result,
			line_token: ress.result[0].line_token
		});
	};
	getrequest_man_by_id = async () => {
		let res = await post('/getrequest_man_by_id', { id: this.props.match.params.id });
		let ress = await post('/get_user_by_id', { id: res.result[0].uid });
		// console.log('ress', ress);
		this.setState({
			dataAll: res.result,
			line_token: ress.result[0].line_token
		});
	};
	getDraw_repair = async () => {
		let tool = await get('/gettools');
		let res = await post('/getdraw_repair', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid)),
			dataTool: res.result.map((e) => {
				let a = Number(tool.result.filter((el) => el.id == e.tid)[0].amount) - Number(e.amount);
				return { id: e.tid, amount: Number(a) };
			}),
			check: res.result
				.map((e) => {
					let a =
						Number(tool.result.filter((el) => el.id == e.tid)[0].amount) >= Number(e.amount) ? true : false;
					return a;
				})
				.find((e) => e == false)
		});
	};
	getDraw_request = async () => {
		let tool = await get('/gettools');
		let res = await post('/getdraw_request', { did: this.props.match.params.id });
		this.setState({
			dataDraw: res.result.sort((a, b) => Number(a.tid) - Number(b.tid)),
			dataTool: res.result.map((e) => {
				let a = Number(tool.result.filter((el) => el.id == e.tid)[0].amount) - Number(e.amount);
				return { id: e.tid, amount: Number(a) };
			}),
			check: res.result
				.map((e) => {
					let a =
						Number(tool.result.filter((el) => el.id == e.tid)[0].amount) >= Number(e.amount) ? true : false;
					return a;
				})
				.find((e) => e == false)
		});
	};
	submit = () => {
		let { check, dataAll } = this.state;
		// console.log('check', check);
		// console.log('this.state', this.state);
		if (check == undefined) {
			swal({
				title: 'Are you sure?',
				text: 'ยืนยันการทำรายการต่อหรือไม่',
				icon: 'warning',
				buttons: true,
				dangerMode: true
			}).then(async (willDelete) => {
				if (willDelete) {
					let { type, id } = this.props.match.params;
					if (type == 1) {
						let res = await post('/repair_success', { id });
						if (res.success) {
							try {
								let obj = {
									message: `
										ขณะนี้คำขอของคุณ
										ดำเนินการเสร็จสิ้นแล้ว
										-----------------------
										รายละเอียดงานเบื้องต้น
										ประเภทงาน : ${dataAll[0].type}
										สถานที่ : scb${dataAll[0].cname}
										อุปกรณ์ที่เกิดปัญหา : ${dataAll[0].device_problem}
										รายละเอียด : ${dataAll[0].detail}
										-----------------------
										เข้าสู่ระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม
										http://itserviceudru.com`,
									token: this.state.line_token
								};
								await post('/noti_line', obj);
							} catch (error) {
								console.log('error', error);
							}
							this.state.dataTool.forEach(async (e, i) => {
								await post('/update_tools', e);
							});
							swal('สำเร็จ!', 'เสร็จสิ้น', 'success', {
								buttons: false,
								timer: 1500
							}).then(() => {
								this.props.history.push('/man_re1');
							});
						} else {
							swal('ผิดพลาด!', 'ยืนยันไม่สำเร็จ', 'error', {
								buttons: false,
								timer: 1500
							});
						}
					} else {
						let res = await post('/request_success', { id });
						if (res.success) {
							try {
								let obj = {
									message: `
										ขณะนี้คำขอของคุณ
										ดำเนินการเสร็จสิ้นแล้ว
										-----------------------
										รายละเอียดงานเบื้องต้น
										ประเภทงาน : ${dataAll[0].type}
										สถานที่ : scb${dataAll[0].cname}
										อุปกรณ์ที่ต้องการเพิ่มเติม : ${dataAll[0].device_problem}
										จำนวนที่ต้องการ : ${dataAll[0].amount}
										รายละเอียด : ${dataAll[0].detail}
										-----------------------
										เข้าสู่ระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม
										http://itserviceudru.com`,
									token: this.state.line_token
								};
								await post('/noti_line', obj);
							} catch (error) {
								console.log('error', error);
							}
							this.state.dataTool.forEach(async (e, i) => {
								await post('/update_tools', e);
							});
							swal('สำเร็จ!', 'เสร็จสิ้น', 'success', {
								buttons: false,
								timer: 1500
							}).then(() => {
								this.props.history.push('/man_re1');
							});
						} else {
							swal('ผิดพลาด!', 'ยืนยันไม่สำเร็จ', 'error', {
								buttons: false,
								timer: 1500
							});
						}
					}
				}
			});
		} else {
			swal('ไม่สามารถดำเนินการได้!', 'กรุณาจัดการข้อมูลการเบิกใหม่อีกครั้ง', 'warning');
		}
	};
	render() {
		let { dataAll, dataDraw, role } = this.state;
		// console.log('dataDraw', dataDraw);
		return (
			<div
				id="form12"
				style={{
					backgroundColor: '#f8f8f8'
				}}
			>
				<Container
					style={{
						paddingTop: '3rem'
					}}
				>
					{dataAll.length > 0 &&
						dataAll.map((e) => (
							<Row
								xs={12}
								sm={6}
								style={{
									width: '90%',
									backgroundColor: 'white',
									padding: '0.30rem 0.30rem 0.30rem 0.30rem',
									borderRadius: '1rem'
								}}
							>
								<Col xs={12} sm={6}>
									<Col
										xs={12}
										sm={12}
										style={{ backgroundColor: 'white', padding: '2px 2px 2px 2px' }}
									>
										<Alert color="info">รายการเบิกอุปกรณ์</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ผู้ดำเนินการซ่อม</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.fname_m + ' ' + e.lname_m}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>สถานะ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{' '}
														{e.status}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>วันที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{moment(e.date).format('DD MMMM') +
															' ' +
															(Number(moment(e.date).format('YYYY')) + 543)}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>เวลา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{moment(e.date).format('HH:mm')}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ประเภท</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.type}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>สถานที่</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{'scb' + e.cname}
													</Label>
												</Col>
											</Col>
										</Row>
										<hr />
										<Row>
											<Col sm={6}>
												<Col>
													<Label>คำขออุปกรณ์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{this.props.match.params.type == 1 ? '-' : e.device_problem}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>จำนวณ</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{this.props.match.params.type == 1 ? '-' : e.amount}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>อุปกรณ์ที่เกิดปัญหา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{this.props.match.params.type == 1 ? e.device_problem : '-'}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>รายละเอียดงาน</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															padding: '0.5rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.detail}
													</Label>
												</Col>
											</Col>
											<Col sm={6}>
												<Col>
													<Label>รูปภาพ</Label>
												</Col>
												<Col>
													<Card inverse style={{ textAlign: 'center' }}>
														{this.props.match.params.type == 1 ? (
															<CardImg
																style={{ width: 'auto', height: '13rem' }}
																src={api + this.props.match.params.id + '.png'}
															/>
														) : (
															<CardImg
																style={{ width: 'auto', height: '13rem' }}
																src={require('../assets/image/default-image.jpg')}
															/>
														)}
													</Card>
												</Col>
											</Col>
										</Row>
									</Col>
								</Col>
								<Col xs={12} sm={6}>
									<Col xs={12} style={{ backgroundColor: 'white', padding: '2px 0px 0px 2px' }}>
										<Alert color="info">ข้อมูลผู้ใช้งาน</Alert>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>ชื่อ-สกุล</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.fname_u + ' ' + e.lname_u}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>ตำแหน่ง</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.position}
													</Label>
												</Col>
											</Col>
										</Row>
										<Row>
											<Col sm={6}>
												<Col>
													<Label>สาขาวิชา</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem'
														}}
													>
														{e.branch}
													</Label>
												</Col>
											</Col>

											<Col sm={6}>
												<Col>
													<Label>โทรศัพท์</Label>
												</Col>
												<Col>
													<Label
														style={{
															backgroundColor: '#f2f2f2',
															width: '200px',
															height: '2rem',
															textAlign: 'center',
															borderRadius: '0.25rem',
															padding: '5px 5px 5px 5px'
														}}
													>
														{e.phone}
													</Label>
												</Col>
											</Col>
										</Row>
									</Col>
									<Col
										xs={12}
										style={{
											backgroundColor: 'white',
											padding: '2px 2px 2px 2px'
										}}
									>
										<Alert color="info">รายการเบิกอุปกรณ์</Alert>
										{dataDraw.length > 0 ? (
											<Table striped responsive style={{ textAlign: 'center' }}>
												<thead>
													<tr>
														<th>ประเภท</th>
														<th>ชื่อ</th>
														<th>จำนวน</th>
													</tr>
												</thead>
												<tbody>
													{dataDraw.map((e) => (
														<tr>
															<td>{e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่'}</td>
															<td>{e.tname}</td>
															<td>{e.amount}</td>
														</tr>
													))}
												</tbody>
											</Table>
										) : (
											<text>---ไม่มีรายการเบิก---</text>
										)}
										{role == 'man' && (
											<div style={{ textAlign: 'center', marginTop: '0.5rem' }}>
												<Button
													color="info"
													style={{ borderRadius: '5rem' }}
													href={
														'/man_re2/' +
														this.props.match.params.type +
														'/' +
														this.props.match.params.id
													}
												>
													จัดการข้อมูลการเบิก
												</Button>
											</div>
										)}
									</Col>
								</Col>
								<div id="btn456">
									<Row>
										<Col
											xs={6}
											style={{
												textAlign: 'right'
											}}
										>
											<Button
												color="danger"
												style={{
													width: '8rem'
												}}
												onClick={() => this.props.history.goBack()}
											>
												ยกเลิก
											</Button>
										</Col>
										<Col xs={6}>
											{role == 'man' && (
												<Button
													color="success"
													style={{
														width: '8rem'
													}}
													onClick={this.submit}
												>
													ยืนยัน
												</Button>
											)}
											{role == 'admin' && (
												<Button
													color="success"
													style={{
														width: '8rem'
													}}
													onClick={() => this.exportXsl()}
												>
													export to crv
												</Button>
											)}
										</Col>
									</Row>
								</div>
							</Row>
						))}
				</Container>
			</div>
		);
	}
	exportXsl = () => {
		let { dataAll, dataDraw } = this.state;
		let download_xls = dataAll.map((e) => ({
			สาขาวิชา: e.branch,
			สถานที่: 'scb' + e.cname,
			วันที่: moment(e.date).add(543, 'year').format('LLL'),
			รายละเอียดงาน: e.detail,
			อุปกรณ์ที่เกิดปัญหา: e.device_problem,
			ผู้ดำเนินการซ่อม: e.fname_m + ' ' + e.lname_m,
			'ชื่อ-สกุล': e.fname_u + ' ' + e.lname_u,
			โทรศัพท์: e.phone,
			ตำแหน่ง: e.position,
			สถานะ: e.status,
			ประเภท: e.type
		}));
		// .concat(dataDraw);
		// console.log('download_xls :', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายการเสร็จสิ้น');
			xls.exportToXLS(
				'รายการเสร็จสิ้นที่' +
					this.props.match.params.id +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
}
