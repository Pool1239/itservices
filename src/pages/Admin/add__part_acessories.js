import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Button,
	FormGroup,
	Label,
	Input,
	Table,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Alert,
	CardImg,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import './add_part.css';
import { get, post, sever } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import img_upload from '../../assets/image/Upload.png';
import base64 from '../../components/base64';
import { async } from 'q';
// let api = sever+'/image/tool/';
let api = sever + '/image/tool/';

export default class add__part_acessories extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			chr: 1,
			tool: [],
			data_obj: {},
			amount: 0,
			type: 1,
			n_name: '',
			n_amount: 0,
			image_set: '',
			currentPage: 0
		};

		this.toggle = this.toggle.bind(this);
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.gettool();
		}
	}
	toggle(e, data) {
		if (e == 1) {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 1,
				image_set: ''
			}));
		} else if (e == 2) {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 2,
				data_obj: data,
				image_set: ''
			}));
		} else {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 3,
				data_obj: data,
				amount: 0
			}));
		}
	}
	gettool = async () => {
		let res = await get('/tool');
		this.setState({
			tool: res.result.map((e) => {
				let type = e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่';
				return {
					...e,
					type: type
				};
			})
		});
	};
	searchText(e) {
		this.gettool().then(() => {
			let { tool } = this.state;
			let texts = e.toLowerCase();
			let a = tool.filter(
				(el) =>
					String(el.type).toLowerCase().indexOf(texts) > -1 ||
					String(el.tname).toLowerCase().indexOf(texts) > -1 ||
					String(el.amount).toLowerCase().indexOf(texts) > -1
			);
			this.setState({ tool: a, currentPage: 0 });
		});
	}
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ image_set: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { chr, tool, data_obj, amount, type, n_amount, image_set, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(tool.length > 0 && tool.length / pageSize);
		return (
			<div id="addpartform" style={{ background: '#f8f8f8' }}>
				<Container style={{ paddingTop: '2rem' }}>
					<Row id="rowaddpart" style={{ width: '90%' }}>
						<Col sm={12}>
							<div>
								<Label id="text2">อะไหล่และอุปกรณ์คงเหลือ</Label>
								<hr />
							</div>
						</Col>

						<Col sm={6}>
							<div>
								<Button color="info" onClick={() => this.toggle(1)}>
									เพิ่มอะไหล่และอุปกรณ์
								</Button>
							</div>
						</Col>
						<Col sm={6}>
							<Input
								type="text"
								onChange={(e) => this.searchText(e.target.value)}
								placeholder="searchExample"
								style={{ width: '250px', float: 'right' }}
							/>
						</Col>

						<Col sm={12} style={{ paddingTop: '1rem' }}>
							<div>
								<Table striped resonsive style={{ textAlign: 'center' }}>
									<thead>
										<tr>
											<th>รูปภาพ</th>
											<th>ประเภท</th>
											<th>ชื่ออะไหล่และอุปกรณ์</th>
											<th>จำนวณคงเหลือ</th>
											<th>จัดการ</th>
										</tr>
									</thead>
									<tbody>
										{tool.length > 0 &&
											tool
												.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
												.map((e) => (
													<tr>
														<td>
															<CardImg
																style={{ width: 'auto', height: '3rem' }}
																src={api + e.id + '.png'}
															/>
														</td>
														<td>{e.type}</td>
														<td>{e.tname}</td>
														<td>{e.amount}</td>
														<td>
															<Button
																size="sm"
																color="success"
																style={{
																	width: '100px'
																}}
																onClick={() => this.toggle(2, e)}
															>
																แก้ไขข้อมูล
															</Button>
															<Button
																size="sm"
																color="info"
																style={{
																	width: '100px',
																	marginLeft: '10px'
																}}
																onClick={() => this.toggle(3, e)}
															>
																เพิ่มจำนวน
															</Button>
															<Button
																size="sm"
																color="danger"
																style={{ width: '100px', marginLeft: '10px' }}
																onClick={() => this.del_tool(e.id)}
															>
																ลบ
															</Button>
														</td>
													</tr>
												))}
									</tbody>
								</Table>
							</div>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
				{chr == 1 ? (
					<Modal isOpen={this.state.modal} toggle={() => this.toggle(1)} className={this.props.className}>
						<ModalBody>
							<Alert color="info">เพิ่มอะไหล่และอุปกรณ์</Alert>
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={3}>
									<Label>เลือกประเภท</Label>
								</Col>
								<Col sm={3}>
									<FormGroup check>
										<Label check>
											<Input
												checked={type == 1 ? true : false}
												value={1}
												onChange={(e) => this.setState({ type: e.target.value })}
												type="radio"
											/>{' '}
											อะไหล่
										</Label>
									</FormGroup>
								</Col>
								<Col sm={3}>
									<FormGroup check>
										<Label check>
											<Input
												checked={type == 1 ? false : true}
												value={2}
												onChange={(e) => this.setState({ type: e.target.value })}
												type="radio"
											/>{' '}
											อุปกรณ์
										</Label>
									</FormGroup>
								</Col>
							</Row>
							<hr />
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={6}>
									<Label for="Na">ชื่ออะไหล่และอุปกรณ์</Label>
									<Input type="text" onChange={(e) => this.setState({ n_name: e.target.value })} />
								</Col>
								<Col sm={6}>
									<Label for="Na2">จำนวน</Label>
									<Input
										value={n_amount}
										onChange={(e) => {
											if (e.target.value < 0) {
												swal('คำเตือน!', 'กรุณากรอกจำนวนเต็ม', 'warning', {
													buttons: false,
													timer: 1500
												});
											} else {
												this.setState({ n_amount: e.target.value });
											}
										}}
										type="number"
										placeholder="0"
									/>
								</Col>
							</Row>
							<hr />
							{image_set !== '' && (
								<div style={{ textAlign: 'center' }}>
									<img src={image_set} style={{ width: '100%', height: '25rem' }} />
								</div>
							)}
							<br />
							<div style={{ textAlign: 'center' }}>
								<label
									style={{
										cursor: 'pointer',
										padding: '0.5rem',
										backgroundColor: '#e9ecef',
										borderRadius: '0.4rem'
									}}
								>
									<input
										type="file"
										onChange={this.uploadImg}
										accept="image/x-png,image/gif,image/jpeg"
									/>
									<img src={img_upload} style={{ width: '1rem', height: '1rem' }} />
									<text>อัปโหลดรูปภาพ</text>
								</label>
							</div>
							<hr />
							<div style={{ textAlign: 'center' }}>
								<Button color="danger" onClick={() => this.toggle(1)}>
									ยกเลิก
								</Button>{' '}
								<Button color="primary" onClick={() => this.insert_tool()}>
									บันทึก
								</Button>
							</div>
						</ModalBody>
					</Modal>
				) : chr == 2 ? (
					<Modal
						isOpen={this.state.modal}
						toggle={() => {
							this.toggle(2, data_obj);
							this.componentDidMount();
						}}
						className={this.props.className}
					>
						{console.log('data_obj', data_obj)}
						<ModalBody>
							<Alert color="info">แก้ไขอะไหล่และอุปกรณ์</Alert>
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={3}>
									<Label>เลือกประเภท</Label>
								</Col>
								<Col sm={3}>
									<FormGroup check>
										<Label check>
											<Input
												checked={data_obj.type === 'อะไหล่' ? true : false}
												// value={1}
												type="radio"
												onChange={() => {
													let { data_obj } = this.state;
													data_obj.type = 'อะไหล่';
													this.setState({ data_obj });
												}}
											/>{' '}
											อะไหล่
										</Label>
									</FormGroup>
								</Col>
								<Col sm={3}>
									<FormGroup check>
										<Label check>
											<Input
												checked={data_obj.type === 'อุปกรณ์' ? true : false}
												// value={2}
												type="radio"
												onChange={() => {
													let { data_obj } = this.state;
													data_obj.type = 'อุปกรณ์';
													this.setState({ data_obj });
												}}
											/>{' '}
											อุปกรณ์
										</Label>
									</FormGroup>
								</Col>
							</Row>
							<hr />
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={12}>
									<Label for="Na">ชื่ออะไหล่และอุปกรณ์</Label>
									<Input
										value={data_obj.tname}
										onChange={(e) => {
											let { data_obj } = this.state;
											data_obj.tname = e.target.value;
											this.setState({ data_obj });
										}}
									/>
								</Col>
							</Row>
							<hr />
							{image_set !== '' && (
								<div style={{ textAlign: 'center' }}>
									<CardImg style={{ width: '12rem', height: '12rem' }} src={image_set} />
								</div>
							)}
							{image_set === '' && (
								<div style={{ textAlign: 'center' }}>
									<CardImg
										style={{ width: '12rem', height: '12rem' }}
										src={api + data_obj.id + '.png'}
									/>
								</div>
							)}
							<br />
							<div style={{ textAlign: 'center' }}>
								<label
									style={{
										cursor: 'pointer',
										padding: '0.5rem',
										backgroundColor: '#e9ecef',
										borderRadius: '0.4rem'
									}}
								>
									<input
										type="file"
										onChange={this.uploadImg}
										accept="image/x-png,image/gif,image/jpeg"
									/>
									<img src={img_upload} style={{ width: '1rem', height: '1rem' }} />
									<text>อัปโหลดรูปภาพ</text>
								</label>
							</div>
							<hr />
							<div style={{ textAlign: 'center' }}>
								<Button
									color="danger"
									onClick={() => {
										this.toggle(2, data_obj);
										this.componentDidMount();
									}}
								>
									ยกเลิก
								</Button>{' '}
								<Button color="primary" onClick={() => this.update_tool_all()}>
									บันทึก
								</Button>
							</div>
						</ModalBody>
					</Modal>
				) : (
					<Modal
						isOpen={this.state.modal}
						toggle={() => {
							this.toggle(3, data_obj);
							this.componentDidMount();
						}}
						className={this.props.className}
					>
						<ModalBody>
							<Alert color="info">เพิ่มจำนวน</Alert>
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={6}>
									<Label>ชื่ออะไหล่และอุปกรณ์</Label>
								</Col>
								<Col sm={6}>
									<Label>: {data_obj.tname} </Label>
								</Col>
							</Row>
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={6}>
									<Label>จำนวนคงเหลือ</Label>
								</Col>
								<Col sm={6}>
									<Label>: {data_obj.amount} </Label>
								</Col>
							</Row>
							<Row style={{ padding: '0.80rem 0.80rem 0.80rem 0.80rem' }}>
								<Col sm={6}>
									<Label>ใส่จำนวนที่เพิ่ม</Label>
								</Col>
								<Col sm={6}>
									<Label>
										<Input
											value={amount}
											type="number"
											onChange={(e) => {
												if (e.target.value < 0) {
													swal('คำเตือน!', 'กรุณากรอกจำนวนเต็ม', 'warning', {
														buttons: false,
														timer: 1500
													});
												} else {
													this.setState({ amount: e.target.value });
												}
											}}
										/>
									</Label>
								</Col>
							</Row>
							<hr />
							<div className="d-flex justify-content-center">
								<CardImg style={{ width: 'auto', height: '3rem' }} src={api + data_obj.id + '.png'} />
							</div>
							<hr />
							<div style={{ textAlign: 'center' }}>
								<Button
									color="danger"
									onClick={() => {
										this.toggle(3, data_obj);
										this.componentDidMount();
									}}
								>
									ยกเลิก
								</Button>{' '}
								<Button color="primary" onClick={() => this.update_tool(data_obj.id, data_obj.amount)}>
									บันทึก
								</Button>
							</div>
						</ModalBody>
					</Modal>
				)}
			</div>
		);
	}
	update_tool_all = async () => {
		let { data_obj, image_set } = this.state;
		let obj = {
			id: data_obj.id,
			type: data_obj.type === 'อุปกรณ์' ? 'tool' : 'spare',
			tname: data_obj.tname,
			image: image_set === '' ? '' : image_set.split(',')[1]
		};
		// console.log('obj', obj);
		let res = await post('/update_tool_all', obj);
		if (res.success) {
			swal('สำเร็จ!', res.message, 'success', {
				buttons: false,
				timer: 1500
			}).then(() => window.location.reload());
		} else {
			swal('ผิดพลาด!', 'แก้ไขอุปกรณ์หรืออะไหล่ไม่สำเร็จ', 'error', {
				buttons: false,
				timer: 1500
			});
		}
	};
	update_tool = async (id, amount_old) => {
		let USER = JSON.parse(localStorage.getItem('myData'));
		let { amount } = this.state;
		let new_amount = Number(amount_old) + Number(amount);
		let res = await post('/update_tool', { id: id, amount: new_amount });
		if (res.success) {
			let obj = {
				tid: id,
				uid: USER.id,
				date: moment(new Date()).format('YYYY-MM-DD HH:mm'),
				amount: amount
			};
			let ress = await post('/insert_draw_tool', obj);
			swal('สำเร็จ!', ress.message, 'success', {
				buttons: false,
				timer: 1500
			}).then(() => window.location.reload());
		} else {
			swal('ผิดพลาด!', 'เพิ่มจำนวนไม่สำเร็จ', 'error', {
				buttons: false,
				timer: 1500
			});
		}
	};
	insert_tool = async () => {
		let { n_amount, n_name, image_set, type } = this.state;
		if (n_name === '' || n_amount === 0) {
			swal('คำเตือน!', 'กรุณากรอกข้อมูลให้ครบ', 'warning', {
				buttons: false,
				timer: 1500
			});
		} else {
			let obj = {
				tname: n_name,
				type: type === 1 ? 'spare' : 'tool',
				amount: n_amount,
				image: image_set !== '' ? image_set.split(',')[1] : base64.img_default.split(',')[1]
			};
			let res = await post('/insert_tool', obj);
			if (res.success) {
				let USER = JSON.parse(localStorage.getItem('myData'));
				let obj = {
					tid: res.id,
					uid: USER.id,
					date: moment(new Date()).format('YYYY-MM-DD HH:mm'),
					amount: n_amount
				};
				let ress = await post('/insert_draw_tool', obj);
				swal('สำเร็จ!', ress.message, 'success', {
					buttons: false,
					timer: 1500
				}).then(() => window.location.reload());
			} else {
				swal('ผิดพลาด!', 'เพิ่มอุปกรณ์หรืออะไหล่ไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		}
	};
	del_tool = async (e) => {
		swal({
			title: 'Are you sure?',
			text: 'คุณต้องการลบอุปกรณ์หรืออะไหล่ใช้หรือไม่!',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let res = await post('/del_tool', { id: e });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success', {
						buttons: false,
						timer: 1500
					}).then(() => this.componentDidMount());
				} else {
					swal('ผิดพลาด!', 'ลบอุปกรณ์หรืออะไหล่ไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		});
	};
}
