import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Button,
	FormGroup,
	Label,
	Input,
	Table,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import './add_location.css';
import { get, post } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import { async } from 'q';

export default class add_location extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			chr: 1,
			getbuilding: [],
			/************************************** */
			bname: '',
			id: null,
			currentPage: 0
		};

		this.toggle = this.toggle.bind(this);
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getbuilding();
		}
	}
	getbuilding = async () => {
		let res = await get('/getbuilding');
		this.setState({ getbuilding: res.result.filter((e) => e.status == 'open') });
	};
	toggle(e) {
		if (e == 1) {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 1,
				bname: ''
			}));
		} else {
			this.setState((prevState) => ({
				modal: !prevState.modal,
				chr: 2,
				bname: ''
			}));
		}
	}
	searchText(e) {
		this.getbuilding().then(() => {
			let { getbuilding } = this.state;
			let texts = e.toLowerCase();
			let a = getbuilding.filter((el) => String(el.bname).toLowerCase().indexOf(texts) > -1);
			this.setState({ getbuilding: a, currentPage: 0 });
		});
	}
	insert_building = async () => {
		let { bname, getbuilding } = this.state;
		if (bname == '') {
			swal('คำเตือน!', 'กรุณากรอกชื่ออาคาร', 'warning', {
				buttons: false,
				timer: 1500
			});
		} else {
			let check = getbuilding.some((e) => e.bname == bname);
			if (check == true) {
				swal('คำเตือน!', 'ชื่ออาคารซ้ำ', 'warning', {
					buttons: false,
					timer: 2000
				});
			} else {
				let res = await post('/insert_building', { bname });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success').then(() => window.location.reload());
				} else {
					swal('ผิดพลาด!', 'ผิดพลาด', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		}
	};
	del_building = async (e) => {
		let id = e;
		swal({
			title: 'Are you sure?',
			text: 'คุณต้องการทำรายการต่อหรือไหม?',
			icon: 'warning',
			buttons: true,
			dangerMode: true
		}).then(async (willDelete) => {
			if (willDelete) {
				let res = await post('/del_building', { id: id });
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success').then(() => this.getbuilding());
				} else {
					swal('ผิดพลาด!', 'ลบไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			}
		});
	};
	update_building = async () => {
		let { bname, id, getbuilding } = this.state;
		let obj = {
			bname,
			id: Number(id)
		};
		let check = getbuilding.some((e) => e.bname == bname);
		if (check == true || bname == '') {
			swal('คำเตือน!', 'กรุณากรอกชื่ออาคารใหม่', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			let res = await post('/update_building', obj);
			if (res.success) {
				swal('สำเร็จ!', res.message, 'success').then(() => {
					this.getbuilding();
					this.toggle(1);
				});
			} else {
				swal('ผิดพลาด!', 'ลบไม่สำเร็จ', 'error', {
					buttons: false,
					timer: 1500
				});
			}
		}
	};
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { chr, getbuilding, bname, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(getbuilding.length > 0 && getbuilding.length / pageSize);
		return (
			<div id="addlocationform" style={{ background: '#f8f8f8' }}>
				<Container style={{ paddingTop: '2rem' }}>
					<Row id="containerlocation" style={{ width: '90%' }}>
						<Col sm={12}>
							<div>
								<Label id="text1">เพิ่มอาคารเรียนและห้องเรียน</Label>
								<hr />
							</div>
							<Row>
								<Col sm={6}>
									<Button color="info" onClick={() => this.toggle(1)}>
										เพิ่มอาคารเรียน
									</Button>
								</Col>
								<Col sm={6}>
									<Input
										type="text"
										onChange={(e) => this.searchText(e.target.value)}
										placeholder="search"
										style={{ width: '250px', float: 'right' }}
									/>
								</Col>
							</Row>
							<Col style={{ marginTop: '1rem' }}>
								<div>
									<Table striped responsive style={{ textAlign: 'center' }}>
										<thead>
											<tr>
												<th>ชื่ออาคารเรียน</th>
												<th style={{ width: '445px' }}>จัดการ</th>
											</tr>
										</thead>
										<tbody>
											{getbuilding.length > 0 &&
												getbuilding
													.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
													.map((e) => (
														<tr>
															<td>{e.bname}</td>
															<td>
																<Button
																	color="success"
																	href={'/add_room/' + e.id}
																	style={{
																		width: '100px'
																	}}
																	size="sm"
																>
																	เพิ่มห้องเรียน
																</Button>
																<Button
																	color="info"
																	style={{
																		width: '100px',
																		marginLeft: '10px'
																	}}
																	size="sm"
																	onClick={() => {
																		this.toggle(2);
																		this.setState({ bname: e.bname, id: e.id });
																	}}
																>
																	แก้ไข
																</Button>
																<Button
																	color="danger"
																	size="sm"
																	style={{ width: '100px', marginLeft: '10px' }}
																	onClick={() => this.del_building(e.id)}
																>
																	ลบ
																</Button>
															</td>
														</tr>
													))}
										</tbody>
									</Table>
								</div>
							</Col>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
				{chr == 1 ? (
					<Modal isOpen={this.state.modal} toggle={() => this.toggle(1)} className={this.props.className}>
						<ModalHeader toggle={() => this.toggle(1)}>เพิ่มอาคารเรียน</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="Na">ชื่ออาคาร</Label>
								<Input
									type="text"
									value={bname}
									onChange={(e) => this.setState({ bname: e.target.value })}
								/>
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="danger" onClick={() => this.toggle(1)}>
								ยกเลิก
							</Button>{' '}
							<Button color="primary" onClick={this.insert_building}>
								บันทึก
							</Button>
						</ModalFooter>
					</Modal>
				) : (
					<Modal isOpen={this.state.modal} toggle={() => this.toggle(2)} className={this.props.className}>
						<ModalHeader toggle={() => this.toggle(2)}>แก้ไขอาคารเรียน</ModalHeader>
						<ModalBody>
							<FormGroup>
								<Label for="Na">ชื่ออาคาร</Label>
								<Input
									type="text"
									value={bname}
									onChange={(e) => this.setState({ bname: e.target.value })}
								/>
							</FormGroup>
						</ModalBody>
						<ModalFooter>
							<Button color="danger" onClick={() => this.toggle(2)}>
								ยกเลิก
							</Button>{' '}
							<Button color="primary" onClick={this.update_building}>
								บันทึก
							</Button>
						</ModalFooter>
					</Modal>
				)}
			</div>
		);
	}
}
