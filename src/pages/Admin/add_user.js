import React, { Component } from 'react';
import { Container, Row, Col, Button, FormGroup, Label, Input, Alert } from 'reactstrap';
import './adduser.css';
import { async } from 'q';
import { post, get } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';

export default class add_user extends Component {
	constructor(props) {
		super(props);

		this.state = {
			role: 'user',
			name: '',
			position: '',
			branch: '',
			phone: '',
			email: '',
			username: '',
			password: '',
			c_password: '',
			prefix: 'นาย',
			line_token: '',
			//********************************************************* */
			check_update: false,
			username_old: ''
		};
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			// console.log(this.props.match.params.id);
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			if (this.props.match.params.id !== undefined) {
				this.getUser(this.props.match.params.id);
			}
		}
	}
	getUser = async (e) => {
		let id = e;
		let res = await get('/user_all');
		let data = res.result.filter((e) => Number(e.id) == Number(id));
		// console.log('data', data);
		this.setState({
			role: data[0].role,
			name: data[0].fname + ' ' + data[0].lname,
			position: data[0].position,
			branch: data[0].branch,
			phone: data[0].phone,
			email: data[0].email,
			username: data[0].username,
			password: data[0].password,
			prefix: data[0].prefix,
			check_update: true,
			username_old: data[0].username,
			line_token: data[0].line_token
		});
	};
	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	create_user = async () => {
		let {
			role,
			name,
			position,
			branch,
			phone,
			email,
			username,
			password,
			c_password,
			prefix,
			check_update,
			username_old,
			line_token
		} = this.state;
		let fname = name.split(' ')[0];
		let lname = name.split(' ')[1];
		let obj = {
			role,
			fname,
			lname,
			position,
			branch,
			phone,
			email,
			username,
			password,
			prefix,
			id: Number(this.props.match.params.id),
			line_token
		};
		// console.log('obj', obj);
		if (
			obj.role === '' ||
			obj.fname === undefined ||
			obj.lname === undefined ||
			obj.position === '' ||
			obj.branch === '' ||
			obj.phone === '' ||
			obj.email === '' ||
			obj.username === '' ||
			obj.password === '' ||
			// c_password === '' ||
			obj.prefix === '' ||
			line_token === ''
		) {
			swal('คำเตือน!', 'กรุณากรอกข้อมูลให้ครบ', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			if (check_update == false) {
				let user = await get('/user_all');
				let check = user.result.some((e) => e.username == username);
				if (check == true) {
					swal('คำเตือน!', 'ชื่อผู้ใช้งานซ้ำ', 'warning', {
						buttons: false,
						timer: 2000
					});
				} else {
					if (password !== c_password) {
						swal('คำเตือน!', 'password ไม่ตรงกัน', 'warning', {
							buttons: false,
							timer: 2000
						});
					} else {
						let res = await post('/insert_user', obj);
						if (res.success) {
							swal('สำเร็จ!', res.message, 'success').then(() => this.props.history.push('/admin_add'));
						} else {
							swal('ผิดพลาด!', 'ผิดพลาด', 'error', {
								buttons: false,
								timer: 1500
							});
						}
					}
				}
			} else {
				if (username == username_old) {
					// if (password !== c_password) {
					// 	swal('คำเตือน!', 'password ไม่ตรงกัน', 'warning', {
					// 		buttons: false,
					// 		timer: 2000
					// 	});
					// } else {
					let res = await post('/update_user', obj);
					if (res.success) {
						swal('สำเร็จ!', res.message, 'success').then(() => this.props.history.push('/admin_add'));
					} else {
						swal('ผิดพลาด!', 'ผิดพลาด', 'error', {
							buttons: false,
							timer: 1500
						});
						// }
					}
				} else {
					let user = await get('/user_all');
					let check = user.result.some((e) => e.username == username);
					if (check == true) {
						swal('คำเตือน!', 'ชื่อผู้ใช้งานซ้ำ', 'warning', {
							buttons: false,
							timer: 2000
						});
					} else {
						// if (password !== c_password) {
						// 	swal('คำเตือน!', 'password ไม่ตรงกัน', 'warning', {
						// 		buttons: false,
						// 		timer: 2000
						// 	});
						// } else {
						let res = await post('/update_user', obj);
						if (res.success) {
							swal('สำเร็จ!', res.message, 'success').then(() => this.props.history.push('/admin_add'));
						} else {
							swal('ผิดพลาด!', 'ผิดพลาด', 'error', {
								buttons: false,
								timer: 1500
							});
						}
						// }
					}
				}
			}
		}
	};
	render() {
		let {
			role,
			name,
			position,
			branch,
			phone,
			email,
			username,
			password,
			c_password,
			prefix,
			line_token
		} = this.state;
		return (
			<div id="aaa" style={{ backgroundColor: '#f8f8f8' }}>
				<Container id="eee">
					<Row style={{ width: '90%' }}>
						<Col xs={12} xl={6}>
							<div id="bbb">
								<Alert color="info">เพิ่มผู้ใช้งานระบบ</Alert>

								<hr />
								<Row>
									<Col xs={12} sm={6}>
										<Label>เลือกประเภทผู้ใช้งาน</Label>
										<Input
											value={role}
											name="role"
											type="select"
											style={{ height: '1.8rem', padding: '2px 2px 2px 2px' }}
											onChange={this.onChange}
										>
											<option value="user">บุคลากร</option>
											<option value="man">ช่างซ่อมบำรุง</option>
										</Input>
									</Col>
									<Col xs={12} sm={6}>
										<Label>คำนำหน้าชื่อ</Label>
										<Input
											value={prefix}
											name="prefix"
											type="select"
											style={{ height: '1.8rem', padding: '2px 2px 2px 2px' }}
											onChange={this.onChange}
										>
											<option value="นาย">นาย</option>
											<option value="นาง">นาง</option>
											<option value="นางสาว">นางสาว</option>
										</Input>
									</Col>
								</Row>
								<Row style={{ paddingTop: '12px' }}>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>ชื่อ-สกุล</Label>
											<Input
												type="text"
												id="ccc"
												name="name"
												onChange={this.onChange}
												value={name}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>ตำแหน่ง</Label>
											<Input
												type="text"
												id="ccc"
												name="position"
												onChange={this.onChange}
												value={position}
											/>
										</FormGroup>
									</Col>
								</Row>
								<Row>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>สาขาวิชา</Label>
											<Input
												type="text"
												id="ccc"
												name="branch"
												value={branch}
												onChange={this.onChange}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>โทรศัพท์</Label>
											<Input
												type="number"
												id="ccc"
												name="phone"
												value={phone}
												onChange={this.onChange}
											/>
										</FormGroup>
									</Col>
								</Row>
								<FormGroup>
									<Label>Email</Label>
									<Input type="text" id="ccc" name="email" value={email} onChange={this.onChange} />
								</FormGroup>
								<FormGroup>
									<Label>Line token</Label>
									<Input
										name="line_token"
										value={line_token}
										onChange={this.onChange}
										type="text"
										id="ccc"
									/>
								</FormGroup>
							</div>
						</Col>
						<Col xs={12} xl={6}>
							<div id="bbb" style={{ height: '100%' }}>
								<Alert color="info">ชื่อผู้ใช้งานและรหัสผ่าน</Alert>
								<hr />
								<Row>
									<Col>
										<Label>ชื่อผู้ใช้งาน</Label>
										<Input
											type="text"
											id="ccc"
											name="username"
											value={username}
											onChange={this.onChange}
										/>
									</Col>
								</Row>
								<Row style={{ paddingTop: '12px' }}>
									<Col>
										<FormGroup>
											<Label>รหัสผ่าน</Label>
											<Input
												type="password"
												name="password"
												id="ccc"
												value={password}
												onChange={this.onChange}
											/>
										</FormGroup>
									</Col>
									<Col>
										<FormGroup>
											<Label>ยืนยันรหัสผ่าน</Label>
											<Input
												type="password"
												id="ccc"
												name="c_password"
												value={c_password}
												onChange={this.onChange}
											/>
										</FormGroup>
									</Col>
								</Row>
							</div>
						</Col>
						<div id="ddd">
							<Row>
								<Col xs={6} style={{ textAlign: 'right' }}>
									<Button color="danger" href="/admin_add" style={{ width: '8rem' }}>
										ยกเลิก
									</Button>
								</Col>
								<Col xs={6}>
									<Button color="success" style={{ width: '8rem' }} onClick={this.create_user}>
										ยืนยัน
									</Button>
								</Col>
							</Row>
						</div>
					</Row>
				</Container>
			</div>
		);
	}
}
