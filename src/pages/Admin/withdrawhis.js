import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Button,
	FormGroup,
	Label,
	Input,
	Table,
	CardImg,
	Pagination,
	PaginationItem,
	PaginationLink
} from 'reactstrap';
import './add_location.css';
import { get, post, sever } from '../../service/service';
import moment from 'moment';
import swal from 'sweetalert';
import XlsExport from 'xlsexport';
// let api = sever + '/image/tool/';
let api = sever + '/image/tool/';

export default class withdrawhis extends Component {
	constructor(props) {
		super(props);

		this.state = {
			dataAll: [],
			currentPage: 0
		};
	}
	componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let th = require('moment/locale/th');
			moment.updateLocale('th', th);
			this.getdata();
		}
	}
	getdata = async () => {
		let rep = await get('/draw_repair');
		let req = await get('/draw_request');
		let data = req.result.concat(rep.result);
		this.setState({
			dataAll: data.sort((a, b) => moment(b.date) - moment(a.date)).map((e) => {
				let date = moment(e.date).format('DD MMMM') + ' ' + (Number(moment(e.date).format('YYYY')) + 543);
				let time = moment(e.date).format('HH:mm');
				let type = e.type == 'tool' ? 'อุปกรณ์' : 'อะไหล่';
				return {
					date: date,
					time: time,
					id: e.tid,
					type: type,
					tname: e.tname,
					amount: e.amount,
					name: e.fname + ' ' + e.lname
				};
			})
		});
	};
	searchText(e) {
		this.getdata().then(() => {
			let { dataAll } = this.state;
			let texts = e.toLowerCase();
			let a = dataAll.filter(
				(el) =>
					String(el.date).toLowerCase().indexOf(texts) > -1 ||
					String(el.time).toLowerCase().indexOf(texts) > -1 ||
					String(el.type).toLowerCase().indexOf(texts) > -1 ||
					String(el.tname).toLowerCase().indexOf(texts) > -1 ||
					String(el.amount).toLowerCase().indexOf(texts) > -1 ||
					String(el.name).toLowerCase().indexOf(texts) > -1
			);
			this.setState({ dataAll: a, currentPage: 0 });
		});
	}
	exportXsl = () => {
		let { dataAll } = this.state;
		let download_xls = dataAll;
		// console.log('download_xls :', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'ประวัติการเบิกอะไหล่และอุปกรณ์');
			xls.exportToXLS(
				'ประวัติการเบิกอะไหล่และอุปกรณ์' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	handleClick(e, index) {
		e.preventDefault();
		this.setState({
			currentPage: index
		});
	}
	render() {
		let { dataAll, currentPage } = this.state;
		let pageSize = 5;
		let pagesCount = Math.ceil(dataAll.length > 0 && dataAll.length / pageSize);
		return (
			<div id="addlocationform" style={{ background: '#f8f8f8' }}>
				<Container style={{ paddingTop: '2rem' }}>
					<Row id="containerlocation" style={{ width: '90%' }}>
						<Col sm={12}>
							<div>
								<Label id="text1">ประวัติการเบิกอะไหล่และอุปกรณ์</Label>
								<hr />
							</div>
							<Row>
								<Col sm={6}>
									<Button color="success" onClick={this.exportXsl}>
										export to crv
									</Button>
								</Col>

								<Col sm={6}>
									<Input
										type="text"
										onChange={(e) => this.searchText(e.target.value)}
										placeholder="search example"
										style={{ width: '250px', float: 'right' }}
									/>
								</Col>
							</Row>
							<Col sm={12} style={{ marginTop: '1rem' }}>
								<div>
									<Table striped responsive style={{ textAlign: 'center' }}>
										<thead>
											<tr>
												<th>วันที่</th>
												<th>เวลา</th>
												<th>รูปภาพ</th>
												<th>ประเภท</th>
												<th>ชื่ออะไหล่และอุปกรณ์</th>
												<th>จำนวน</th>
												<th>ชื่อผู้เบิก</th>
											</tr>
										</thead>
										<tbody>
											{dataAll.length > 0 &&
												dataAll
													.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
													.map((e) => (
														<tr>
															<td>{e.date}</td>
															<td>{e.time}</td>
															<td>
																<CardImg
																	style={{ width: 'auto', height: '3rem' }}
																	src={api + e.id + '.png'}
																/>
															</td>
															<td>{e.type}</td>
															<td>{e.tname}</td>
															<td>{e.amount}</td>
															<td>{e.name}</td>
														</tr>
													))}
										</tbody>
									</Table>
								</div>
							</Col>
						</Col>
						<Col sm={12}>
							<br />
							<div
								className="pagination-wrapper"
								style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
							>
								<Pagination aria-label="Page navigation example">
									<PaginationItem disabled={currentPage <= 0}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage - 1)}
											previous
											href="#"
										/>
									</PaginationItem>

									{[ ...Array(pagesCount) ].map((page, i) => (
										<PaginationItem active={i === currentPage} key={i}>
											<PaginationLink onClick={(e) => this.handleClick(e, i)} href="#">
												{i + 1}
											</PaginationLink>
										</PaginationItem>
									))}

									<PaginationItem disabled={currentPage >= pagesCount - 1}>
										<PaginationLink
											onClick={(e) => this.handleClick(e, currentPage + 1)}
											next
											href="#"
										/>
									</PaginationItem>
								</Pagination>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
