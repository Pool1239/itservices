import React, { Component } from 'react';
import { Container, Row, Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './edit_user.css';
import swal from 'sweetalert';
import { get, post } from '../../service/service';

export default class Edit_user extends Component {
	constructor(props) {
		super(props);

		this.state = {
			branch: '',
			email: '',
			fullname: '',
			phone: '',
			position: '',
			username: '',
			usernameold: '',
			password: '',
			line_token: '',
			id: '',
			//---------------------
			pass1: '',
			pass2: '',
			passOld: '',
			btn_radio: 0
		};
	}
	componentDidMount = async () => {
		let res = localStorage.getItem('myData');
		if (res == null) {
			this.props.history.push('/');
		} else {
			let myData = JSON.parse(localStorage.getItem('myData'));
			let res = await post('/getuser', { id: myData.id });
			this.setState({
				branch: res.result[0].branch,
				email: res.result[0].email,
				phone: res.result[0].phone,
				position: res.result[0].position,
				fullname: res.result[0].fname + ' ' + res.result[0].lname,
				username: res.result[0].username,
				usernameold: res.result[0].username,
				password: res.result[0].password,
				line_token: res.result[0].line_token,
				id: myData.id
			});
		}
	};
	updateUser = async () => {
		let {
			branch,
			email,
			phone,
			position,
			fullname,
			username,
			password,
			id,
			pass1,
			pass2,
			passOld,
			btn_radio,
			line_token,
			usernameold
		} = this.state;
		let fname = fullname.split(' ')[0];
		let lname = fullname.split(' ')[1];
		if (btn_radio == 1) {
			let obj = {
				branch,
				email,
				phone,
				position,
				fname,
				lname,
				username,
				password,
				id,
				line_token
			};
			try {
				let res = await post('/updateData', obj);
				if (res.success) {
					swal('สำเร็จ!', res.message, 'success', {
						buttons: false,
						timer: 1500
					}).then(() => {
						this.props.history.push('/home_user');
					});
				} else {
					swal('ผิดพลาด!', 'แก้ไขไม่สำเร็จ', 'error', {
						buttons: false,
						timer: 1500
					});
				}
			} catch (error) {
				console.log(error);
			}
		} else if (btn_radio == 2) {
			if (usernameold == username) {
				if (password !== passOld) {
					swal('ผิดพลาด!', 'รหัสผ่านปัจจุบันไม่ถูกต้อง', 'error', {
						buttons: false,
						timer: 1500
					});
				} else {
					let obj = {
						branch,
						email,
						phone,
						position,
						fname,
						lname,
						username,
						password,
						id,
						line_token
					};
					try {
						let res = await post('/updateData', obj);
						if (res.success) {
							swal('สำเร็จ!', res.message, 'success', {
								buttons: false,
								timer: 1500
							}).then(() => {
								localStorage.clear();
								this.props.history.push('/');
							});
						} else {
							swal('ผิดพลาด!', 'แก้ไขไม่สำเร็จ', 'error', {
								buttons: false,
								timer: 1500
							});
						}
					} catch (error) {
						console.log(error);
					}
				}
			} else {
				let user = await get('/user_all');
				let check = user.result.some((e) => e.username == username);
				if (check == true) {
					swal('คำเตือน!', 'ชื่อผู้ใช้งานซ้ำ', 'warning', {
						buttons: false,
						timer: 2000
					});
				} else {
					if (password !== passOld) {
						swal('ผิดพลาด!', 'รหัสผ่านปัจจุบันไม่ถูกต้อง', 'error', {
							buttons: false,
							timer: 1500
						});
					} else {
						let obj = {
							branch,
							email,
							phone,
							position,
							fname,
							lname,
							username,
							password,
							id,
							line_token
						};
						try {
							let res = await post('/updateData', obj);
							if (res.success) {
								swal('สำเร็จ!', res.message, 'success', {
									buttons: false,
									timer: 1500
								}).then(() => {
									localStorage.clear();
									this.props.history.push('/');
								});
							} else {
								swal('ผิดพลาด!', 'แก้ไขไม่สำเร็จ', 'error', {
									buttons: false,
									timer: 1500
								});
							}
						} catch (error) {
							console.log(error);
						}
					}
				}
			}
		} else if (btn_radio == 3) {
			if (password !== passOld) {
				swal('ผิดพลาด!', 'รหัสผ่านปัจจุบันไม่ถูกต้อง', 'error', {
					buttons: false,
					timer: 1500
				});
			} else {
				if (pass1 !== pass2) {
					swal('ผิดพลาด!', 'รหัสผ่านไม่ตรงกัน', 'error', {
						buttons: false,
						timer: 1500
					});
				} else {
					if (pass1 == '' || pass2 == '') {
						swal('ผิดพลาด!', 'กรอกรหัสผ่านให้ครบ', 'error', {
							buttons: false,
							timer: 1500
						});
					} else {
						let obj = {
							branch,
							email,
							phone,
							position,
							fname,
							lname,
							username,
							password: pass1,
							id,
							line_token
						};
						try {
							let res = await post('/updateData', obj);
							if (res.success) {
								swal('สำเร็จ!', res.message, 'success', {
									buttons: false,
									timer: 1500
								}).then(() => {
									localStorage.clear();
									this.props.history.push('/');
								});
							} else {
								swal('ผิดพลาด!', 'แก้ไขไม่สำเร็จ', 'error', {
									buttons: false,
									timer: 1500
								});
							}
						} catch (error) {
							console.log(error);
						}
					}
				}
			}
		} else {
			swal('ผิดพลาด!', 'กรุณาทำการแก้ไขข้อมูลก่อน', 'error', {
				buttons: false,
				timer: 1500
			});
		}
	};
	render() {
		let { branch, email, phone, position, fullname, username, btn_radio, line_token } = this.state;
		return (
			<div id="a" style={{ backgroundColor: '#f8f8f8' }}>
				<Container id="e">
					<Row style={{ width: '90%' }}>
						<Col xs={12} xl={6}>
							<div id="b" style={{ height: '100%' }}>
								<p>
									<FormGroup check>
										<Label check>
											<Input
												type="radio"
												name="radio1"
												onChange={() => this.setState({ btn_radio: 1 })}
											/>
											แก้ไขข้อมูล
										</Label>
									</FormGroup>
								</p>
								<hr />
								<Row>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>ชื่อ-สกุล</Label>
											<Input
												type="text"
												id="c"
												value={fullname}
												disabled={btn_radio == 0 || btn_radio == 2 || btn_radio == 3}
												onChange={(e) => this.setState({ fullname: e.target.value })}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>ตำแหน่ง</Label>
											<Input
												type="text"
												id="c"
												value={position}
												disabled={btn_radio == 0 || btn_radio == 2 || btn_radio == 3}
												onChange={(e) => this.setState({ position: e.target.value })}
											/>
										</FormGroup>
									</Col>
								</Row>
								<Row>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>สาขาวิชา</Label>
											<Input
												type="text"
												id="c"
												value={branch}
												disabled={btn_radio == 0 || btn_radio == 2 || btn_radio == 3}
												onChange={(e) => this.setState({ branch: e.target.value })}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={6}>
										<FormGroup>
											<Label>โทรศัพท์</Label>
											<Input
												type="number"
												id="c"
												value={phone}
												disabled={btn_radio == 0 || btn_radio == 2 || btn_radio == 3}
												onChange={(e) => this.setState({ phone: e.target.value })}
											/>
										</FormGroup>
									</Col>
								</Row>
								<FormGroup>
									<Label>Email</Label>
									<Input
										type="text"
										id="c"
										value={email}
										disabled={btn_radio == 0 || btn_radio == 2 || btn_radio == 3}
										onChange={(e) => this.setState({ email: e.target.value })}
									/>
								</FormGroup>
								<FormGroup>
									<Label>Line token</Label>
									<Input
										value={line_token}
										onChange={(e) => this.setState({ line_token: e.target.value })}
										type="text"
										id="c"
										disabled={btn_radio == 0 || btn_radio == 2 || btn_radio == 3}
									/>
								</FormGroup>
							</div>
						</Col>
						<Col xs={12} xl={6}>
							<div id="b" style={{ height: '100%' }}>
								<p>
									<FormGroup check>
										<Label check>
											<Input
												type="radio"
												name="radio1"
												onChange={() => this.setState({ btn_radio: 2 })}
											/>
											แก้ไขชื่อผู้ใช้งาน
										</Label>
									</FormGroup>
								</p>
								<hr />
								<FormGroup>
									<Label>ชื่อผู้ใช้งาน</Label>
									<Input
										type="text"
										id="c"
										value={username}
										disabled={btn_radio == 0 || btn_radio == 1 || btn_radio == 3}
										onChange={(e) => this.setState({ username: e.target.value })}
									/>
								</FormGroup>
								<FormGroup>
									<Label>ยืนยันรหัสผ่านปัจจุบัน</Label>
									<Input
										type="password"
										id="c"
										disabled={btn_radio == 0 || btn_radio == 1 || btn_radio == 3}
										onChange={(e) => this.setState({ passOld: e.target.value })}
									/>
								</FormGroup>
								<br />
								<p>
									<FormGroup check>
										<Label check>
											<Input
												type="radio"
												name="radio1"
												onChange={() => this.setState({ btn_radio: 3 })}
											/>
											แก้ไขรหัสผ่าน
										</Label>
									</FormGroup>
								</p>
								<hr />
								<FormGroup>
									<Label>รหัสผ่านปัจจุบัน</Label>
									<Input
										type="password"
										id="c"
										disabled={btn_radio == 0 || btn_radio == 1 || btn_radio == 2}
										onChange={(e) => this.setState({ passOld: e.target.value })}
									/>
								</FormGroup>
								<FormGroup>
									<Label>รหัสผ่านใหม่</Label>
									<Input
										type="password"
										id="c"
										disabled={btn_radio == 0 || btn_radio == 1 || btn_radio == 2}
										onChange={(e) => this.setState({ pass1: e.target.value })}
									/>
								</FormGroup>
								<FormGroup>
									<Label>พิมพ์รหัสผ่านใหม่อีกครั้ง</Label>
									<Input
										type="password"
										id="c"
										disabled={btn_radio == 0 || btn_radio == 1 || btn_radio == 2}
										onChange={(e) => this.setState({ pass2: e.target.value })}
									/>
								</FormGroup>
							</div>
						</Col>
						<div id="d">
							<Row>
								<Col xs={6} style={{ textAlign: 'right' }}>
									<Button
										color="danger"
										style={{ width: '8rem' }}
										onClick={() => this.props.history.push('/home_user')}
									>
										ยกเลิก
									</Button>
								</Col>
								<Col xs={6}>
									<Button color="success" style={{ width: '8rem' }} onClick={this.updateUser}>
										ยืนยัน
									</Button>
								</Col>
							</Row>
						</div>
					</Row>
				</Container>
			</div>
		);
	}
}
