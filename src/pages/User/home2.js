import React, { Component } from "react";
import {
  Table,

 
  Input,
 
  Row,
  Col,
  Container,
  FormGroup
} from "reactstrap";
import "./userhome.css";
export default class home2 extends Component {
  render() {
    return (
      <div id="form" style={{ backgroundColor: "#f8f8f8" }}>
        <Container style={{ paddingTop: "3rem" }}>
          <Row id="container1" style={{ width: "90%" }}>
            <Col sm={6}>
              <Row>
                <Col sm={6}>
                  <FormGroup>
                    <Input type="select" name="select">
                      <option>รายการซ่อม</option>
                      <option>รายการขออุปกรณ์</option>
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
            </Col>
            <Col sm={6}>
              <Input
                type="search"
                name="search"
                id="exampleSearch"
                placeholder="search placeholder"
              />
            </Col>
            <Col>
              <div style={{ paddingTop: "1rem" }}>
                <Table striped responsive>
                  <thead>
                    <tr>
                      <th>วันที่</th>
                      <th>เวลา</th>
                      <th>สถานที่</th>
                      <th>อุปกรณ์ที่ต้องการ</th>
                      <th>จำนวณ</th>
                      <th>ปัญหาที่พบ</th>
                      <th>สถานะ</th>
                      <th>จัดการ</th>
                      <th />
                    </tr>
                  </thead>
                  <tbody />
                </Table>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
