import React from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap';
import './navbar.css';
import swal from 'sweetalert';
import { get, post } from '../service/service';

export default class navbar extends React.Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			isOpen: false,
			role: '',
			datauser: {}
		};
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	logout() {
		localStorage.clear();
	}
	async componentDidMount() {
		let res = localStorage.getItem('myData');
		if (res == null) {
			swal('คำเตือน!', 'กรุณาล็อคอินก่อน', 'warning', {
				buttons: false,
				timer: 2000
			});
		} else {
			let myData = JSON.parse(localStorage.getItem('myData'));
			let res = await post('/getuser', { id: myData.id });
			// console.log('res', res);

			this.setState({
				role: myData.role,
				datauser: {
					name: res.result[0].fname + ' ' + res.result[0].lname
				}
			});
		}
	}
	render() {
		let { role, datauser } = this.state;
		return (
			<Navbar id="nav" light expand="md" style={{ backgroundColor: ' #6699cc' }}>
				<NavbarBrand style={{ fontSize: '1.50rem', fontWeight: '700', color: 'white' }}>
					IT services
				</NavbarBrand>
				<NavbarToggler onClick={this.toggle} />
				<Collapse isOpen={this.state.isOpen} navbar>
					<Nav className="ml-auto" navbar>
						{role !== '' &&
						role == 'user' && (
							<div id="navuser">
								<NavItem>
									<NavLink href="/home_user" id="a1">
										หน้าแรก
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/user_repair" id="a2">
										แจ้งซ่อมอุปกรณ์
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/user_request" id="a3">
										ขออุปกรณ์เพิ่มเติม
									</NavLink>
								</NavItem>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav id="a4">
										{datauser.name}
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem>
											<a href="/edit_user">แก้ไขข้อมูลส่วนตัว</a>
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem href="/" onClick={this.logout}>
											ออกจากระบบ
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</div>
						)}
						{role !== '' &&
						role == 'man' && (
							<div id="navuser">
								<NavItem>
									<NavLink href="/man_home1" id="a2">
										หน้าแรก
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/man_re1" id="a2">
										กำลังดำเนินการ
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/manComWork1" id="a2">
										ดำเนินการเสร็จสิ้น
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/man_ac1" id="a2">
										อะไหล่และอุปกรณ์
									</NavLink>
								</NavItem>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav id="a4">
										{datauser.name}
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem>
											<a href="/man_edit">แก้ไขข้อมูลส่วนตัว</a>
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem href="/" onClick={this.logout}>
											ออกจากระบบ
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</div>
						)}
						{role !== '' &&
						role == 'admin' && (
							<div id="navuser">
								<NavItem>
									<NavLink href="/admin_home" id="a2">
										หน้าแรก
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/admin_add" id="a2">
										ข้อมูลผู้ใช้งาน
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/add_location" id="a2">
										อาคารและห้องเรียน
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/add_part" id="a2">
										อะไหล่และอุปกรณ์คงเหลือ
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/withdraw_tool" id="a2">
										ประวัติการเพิ่มอะไหล่และอุปกรณ์
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/withdrawhis" id="a2">
										ประวัติการเบิกอะไหล่และอุปกรณ์
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href="/" id="a2" onClick={this.logout}>
										ออกจากระบบ
									</NavLink>
								</NavItem>
							</div>
						)}
					</Nav>
				</Collapse>
			</Navbar>
		);
	}
}
