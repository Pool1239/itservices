import React from 'react';
import { Route } from 'react-router-dom';
import Start from '../Start';
import { BrowserRouter, Switch } from 'react-router-dom';
import home_user from '../pages/User/Home';
import home_user2 from '../pages/User/home2';
import user_repair from '../pages/User/user_repair';
import user_request from '../pages/User/user_request';
import edit_user from '../pages/User/edit_user';
import man_home1 from '../man/man_home1';
import man_re3 from '../man/man_re3';
import man_ac1 from '../man/man_ac1';
import man_ac2 from '../man/man_ac2';
import manComWork1 from '../man/manComWork1';
import man_re1 from '../man/man_re1';
import man_re2 from '../man/man_re2';
import man_edit from '../man/man_edit';
import admin_home from '../pages/Admin/Home';
import admin_add from '../pages/Admin/add_user_man';
import add_user from '../pages/Admin/add_user';

import add_location from '../pages/Admin/add_location';
import add_part from '../pages/Admin/add__part_acessories';
import withdrawhis from '../pages/Admin/withdrawhis';
import manComWork2 from '../man/manComWork2';
import add_room from '../pages/Admin/add_room';
import admin_edituser from '../pages/Admin/admin_edituser';
import detailWork from '../components/detailWork';
import withdraw_tool from '../pages/Admin/withdraw_tool';

export default () => (
	<Start>
		{/**------------------------------------User------------------------------------------------------------------- */}
		<Route exact path="/home_user" component={home_user} />
		<Route exact path="/home_user2" component={home_user2} />
		<Route exact path="/user_repair" component={user_repair} />
		<Route exact path="/user_request" component={user_request} />
		<Route exact path="/edit_user" component={edit_user} />
		{/**------------------------------------Man------------------------------------------------------------------- */}
		<Route exact path="/man_home1" component={man_home1} />
		<Route exact path="/man_ac1" component={man_ac1} />
		<Route exact path="/man_ac2" component={man_ac2} />
		<Route exact path="/manComWork1" component={manComWork1} />
		<Route exact path="/manComWork2" component={manComWork2} />
		<Route exact path="/man_re1" component={man_re1} />
		<Route exact path="/man_re2/:type/:id" component={man_re2} />
		<Route exact path="/man_edit" component={man_edit} />
		{/**------------------------------------Admin------------------------------------------------------------------- */}
		<Route exact path="/admin_home" component={admin_home} />
		<Route exact path="/admin_add" component={admin_add} />
		<Route exact path="/add_user/:id" component={add_user} />
		<Route exact path="/add_user" component={add_user} />
		<Route exact path="/admin_edituser" component={admin_edituser} />
		<Route exact path="/add_location" component={add_location} />
		<Route exact path="/add_part" component={add_part} />
		<Route exact path="/withdrawhis" component={withdrawhis} />
		<Route exact path="/add_room/:id" component={add_room} />
		<Route exact path="/withdraw_tool" component={withdraw_tool} />
		{/**------------------------------------------------------------------------------------------------------- */}
		<Route exact path="/detailWork/:type/:id" component={detailWork} />
		<Route exact path="/man_re3/:type/:id" component={man_re3} />
	</Start>
);
