import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import App from './App';
import login from '../pages/Login/Login';
import homepage from '../components/Homepage';
import detailWork_homepage from '../components/detailWork_homepage';
import detailWork_homepage2 from '../components/detailWork_homepage2';

export default () => (
	<BrowserRouter>
		<Switch>
			<Route exact path="/" component={homepage} />
			<Route exact path="/detailWork_homepage/:type/:id" component={detailWork_homepage} />
			<Route exact path="/detailWork_homepage2/:type/:id" component={detailWork_homepage2} />
			{/* <Route exact path="/" component={login} /> */}
			<Route exact path="/login" component={login} />
			<App />
		</Switch>
	</BrowserRouter>
);
